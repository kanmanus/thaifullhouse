$('.btn-delete').click(function(){
    data_id = $(this).data('id');
    bootbox.confirm("Do you want to delete this list?", function(result) {
        if(result == true){
            $('.form-delete[parent-data-id='+data_id+']').submit();
        }
    });
});

jQuery(document).ready(function(){
    ChangeActive(jQuery('.change-active'));
    ChangeReportStatus(jQuery('.change-report'));


});


function ChangeActive(element)
{
    element.on('click',function(event){

        var tb_name = $(this).data('tb_name');
        var pk_field = $(this).data('pk_field');
        var v_pk_field = $(this).data('v_pk_field');
        var change_field = $(this).data('change_field');
        var value = $(this).data('value');
        var _token = $('input[name=_token]').val();
        var FormData = {
            'tb_name' : tb_name,
            'pk_field' : pk_field,
            'v_pk_field' : v_pk_field,
            'change_field' : change_field,
            'value' : value,
            '_token' : _token
        };

        //console.log('tb_name: '+tb_name+' pk_field: '+pk_field+' v_pk_field: '+v_pk_field+' change_field: '+change_field+' value: '+value);

        $.ajax({
            type : "POST",
            url : 'api-active',
            data : FormData,
            dataType : 'json',
            success: function(data){
                $("#zip_id").append(data);
                if(data==1)
                {
                    //console.log(data);
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('text-red').addClass('text-green');
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('fa-close').addClass('fa-check');
                    $('a[id='+change_field+'-'+v_pk_field+']').data('value','1');
                    value = 1 ;
                    //$().empty().append();
                }else if(data==0)
                {
                    //console.log(data);
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('text-green').addClass('text-red');
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('fa-check').addClass('fa-close');
                    $('a[id='+change_field+'-'+v_pk_field+']').data('value','0');
                    value = 0 ;
                }




            }
        });


    });
}

function ChangeReportStatus(element)
{
    element.on('click',function(event){

        var tb_name = $(this).data('tb_name');
        var pk_field = $(this).data('pk_field');
        var v_pk_field = $(this).data('v_pk_field');
        var change_field = $(this).data('change_field');
        var value = $(this).data('value');
        var _token = $('input[name=_token]').val();
        var FormData = {
            'tb_name' : tb_name,
            'pk_field' : pk_field,
            'v_pk_field' : v_pk_field,
            'change_field' : change_field,
            'value' : value,
            '_token' : _token
        };

        //console.log('tb_name: '+tb_name+' pk_field: '+pk_field+' v_pk_field: '+v_pk_field+' change_field: '+change_field+' value: '+value);

        $.ajax({
            type : "POST",
            url : '/api-report',
            data : FormData,
            success: function(data){
                if(data=='N')
                {
                    //console.log(data);

                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('text-green').addClass('text-red');
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('fa-check').addClass('fa-close');
                    $('a[id='+change_field+'-'+v_pk_field+']').data('value','N');
                    value = 'N' ;
                    //$().empty().append();
                }else if(data=='Y')
                {
                    //console.log(data);

                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('text-red').addClass('text-green');
                    $('a[id='+change_field+'-'+v_pk_field+']').removeClass('fa-close').addClass('fa-check');
                    $('a[id='+change_field+'-'+v_pk_field+']').data('value','Y');
                    value = 'Y' ;
                }


            }
        });


    });


}

<!-- Chk Upload -->
$(document).ready(function(){
    $("#remove_img").click(function(){
        var r = confirm('Are you sure you want to delete');
        if(r == true){
            $("#img_path2").removeAttr('src');
            $("#upload").show();
            $("#remove_img").hide();
            $("#img_path,#img_type").val('');
        }else{
            return false;
        }

    });
});

$(function () {
    $("#upload").on("click",function(e){
        var objFile= $("<input>",{
            "class":"file_upload",
            "type":"file",
            "multiple":"true",
            "name":"img_path",
            "style":"display:none",
            change: function(e){
                var files = this.files
                showThumbnail(files)
                $("#upload").hide();
                $("#remove_img").show();
                $("#img_type").val(1);
                $("#img_path").val('');
            }
        });
        $(this).before(objFile);
        $(".file_upload:last").show().click().hide();
        e.preventDefault();
    });

    function showThumbnail(files){

        //    $("#thumbnail").html("");
        for(var i=0;i<files.length;i++){
            var file = files[i]
            var imageType = /image.*/
            if(!file.type.match(imageType)){
                var i = confirm("สกุลไฟล์ไม่ถูกต้อง");
                if(i==true || i==false){
                    exit();
                }
                continue;
            }


            //var image = document.createElement("img");
            var image = document.getElementById("img_path2");
            var thumbnail = document.getElementById("thumbnail");
            image.file = file;
            thumbnail.appendChild(image)

            var reader = new FileReader()
            reader.onload = (function(aImg){
                return function(e){
                    aImg.src = e.target.result;
                };
            }(image))

            var ret = reader.readAsDataURL(file);
            var canvas = document.createElement("canvas");
            ctx = canvas.getContext("2d");
            image.onload= function(){
                ctx.drawImage(image,100,100)
            }
        } // end for loop

    } // end showThumbnail
});
<!-- End Upload -->

$(document).ready(function(){
    $("#remove_pdf1").click(function(){
        var r = confirm('Are you sure you want to delete');
        if(r == true){
            $("#upload_pdf1").show();
            $("#remove_pdf1,#pdf_name1").hide();
            $("#pdf_path").val('');
        }else{
            return false;
        }

    });
});
