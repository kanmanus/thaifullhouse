
// Stop Close Dropdown Menu
$('.dropdown-menu input, .dropdown-menu label').click(function(e) {
  e.stopPropagation();
});

// Filter For Sell and For Rent
$('#label-search-sell').click(function() {
  if (!$('#label-search-rent hr').hasClass('hidden')) {
    $('#label-search-sell hr').removeClass('hidden');
    $('#label-search-rent hr').addClass('hidden');
    $('#search-sell').removeClass('hidden');
    $('#search-rent').addClass('hidden');
  }
});

$('#label-search-rent').click(function() {
  if (!$('#label-search-sell hr').hasClass('hidden')) {
    $('#label-search-rent hr').removeClass('hidden');
    $('#label-search-sell hr').addClass('hidden');
    $('#search-rent').removeClass('hidden');
    $('#search-sell').addClass('hidden');
  }
});

// Filter All Buy Rent
$('.buy-rent-all span').click(function() {
  if (!$(this).hasClass('active')) {
    $(this).addClass('active');
    $(this).siblings().removeClass('active');
  }
})

// InitMaps Function
function initMaps() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: {lat: 13.745831, lng: 100.551389}
  });

  loopAddMarker(latAndLng);
  setMapOnAll(map);

  google.maps.event.addListener(infoWindow, 'domready', function() {
    changeStyleInfoWindow(map);
  });

}

function changeStyleInfoWindow(map) {
   // Reference to the DIV which receives the contents of the infowindow using jQuery
   var iwOuter = $('.gm-style-iw');

   $.map(iwOuter, function(iwData) {
     $(iwData).next().css({'display': 'none'})

     iwOuter.siblings().css({'display' : 'none'});

     var iwBackground = $(iwData).prev();

     // Remove the background shadow DIV
     iwBackground.children(':nth-child(2)').css({'display' : 'none'});

     // Remove the white background DIV
     iwBackground.children(':nth-child(4)').css({'display' : 'none'});

     $(iwData).click(function(e) {
        e.preventDefault();
        var index = $(iwData.children[0].children[0].children[0]).data('index');
        map.setCenter(markers[index].getPosition());

        closeAllInfo(iwData);

        if (!$(this).hasClass('has-active-marker')) {
          $(this).addClass('has-active-marker');
          $(this).parent().append(imageDatas[index]);
        } else {
          $(this).removeClass('has-active-marker');
          $(this).siblings(":last").remove();
        }

      });
   });
}

function closeAllInfo(iwData) {
  $.map($(iwData).parent().siblings(), function(datas) {
    $.map($(datas), function(data) {
      var infoWin = $(data).children(':nth-child(2)')
      if (infoWin.hasClass('has-active-marker')) {
        infoWin.removeClass('has-active-marker');
        infoWin.siblings(":last").remove();
      }
    });
  });
}

function addMarker(location) {
  var marker = new google.maps.Marker({
    position: location,
    map: map
  });
  markers.push(marker);
}

function loopAddMarker(locationList) {
  for (var i = 0; i < locationList.length; i++) {
    addMarker(locationList[i]);
  }
}

function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
    markers[i].setVisible(false);
    setInfoWindow(map, markers[i], details[i]);
  }
}

function setInfoWindow(map, marker, detail) {
  infoWindow = new google.maps.InfoWindow({
    content: detail
  });

  infoWindow.open(map, marker);
}

// Button Change between List and Picture
$('.btn-result-list').click(function() {
  $('.btn-pic').removeClass('active');
  $('#result-list').removeClass('hidden');
  $('#result-pic').addClass('hidden');
  // $('#pagination').addClass('hidden');
  $(this).addClass('active');
})

$('.btn-pic').click(function() {
  $('.btn-result-list').removeClass('active');
  $('#result-pic').removeClass('hidden');
  $('#result-list').addClass('hidden');
  $(this).addClass('active');
})

// Change Wishlish Icon
$('.wishlish-icon')
  .mouseover(function() {
    var src = "images/icon/ชื่นชอบ2.png";
    $(this).attr("src", src);
  })
  .mouseout(function() {
    var src = "images/icon/ชื่นชอบ.png";
    $(this).attr("src", src);
  })

// Instantiate the Bootstrap carousel
$('.multi-item-carousel').carousel({
  interval: false
});

// for every slide in carousel, copy the next slide's item in the slide.
// Do the same for the next, next item.
$('.multi-item-carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  if (next.next().length>0) {
    next.next().children(':first-child').clone().appendTo($(this));
  } else {
    $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
  }
});
