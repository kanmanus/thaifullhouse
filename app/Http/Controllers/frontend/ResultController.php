<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Request;
use Input;
use Log;
use App\Models\Project;
use App\Models\Product;
use App\Models\ProductType;
use App\Models\LandDevelopment;

class ResultController extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function __construct(){

    }

    public function getResults($activity){
      $text = "text";
      $product_result = Product::join('product_type', 'product.product_type_id', '=', 'product_type.product_type_id')
                        ->where('activity', $activity)
                        ->whereIn('product.product_type_id',[10,11,12,13,14,15,16,17])
                        ->whereNotBetween('product.product_type_id', [1, 9])
                        ->orderBy('product.updated_at', 'desc')
                        ->paginate(1);
                        // ->orderBy('updated_at', 'desc')
                        // ->get();

      return view('frontend.result', compact('product_result'));

    }

    public function getResultsPost(){
      //  $request_decode = json_decode($request_all);
      //  $list = json_encode(Input::get('list'));
      //  $bedrooms = json_encode(Input::get('bedroooms'));
      //  $bathrooms = json_encode(Input::get('bathrooms'));
      //  $varArray  = $value->property;
      // $uri = Request::path();
      // $url = Request::url();
      // $segment_url = Request::segment(0);
      // $all = json_encode(Input::all());
      $property = Input::get('property');
      $lists = Input::get('list');
      $bedrooms = Input::get('bedrooms');
      $bathrooms = Input::get('bathrooms');
      $activity = Input::get('activity');
      $page = Input::get('page');

      $product_result = Product::join('product_type', 'product.product_type_id', '=', 'product_type.product_type_id');
      $product_result->where('product.activity',$activity);
      $product_result->whereIn('product.product_type_id',$property);
      if(!empty($bedrooms))
      $product_result->where('product.bed','<=',$bedrooms);
      if(!empty($bathrooms))
      $product_result->where('product.bath','<=',$bathrooms);
      $product_result->orderBy('product.updated_at', 'desc');
      // $products = $product_result->skip(1)->take(1)->get();
      $count_record = $product_result->count();


      $page_number = $page;
      $item_per_page = 1;
      $page_current = $page;
      $total_pages = ceil($count_record/$item_per_page);
      $skiper = (($page_number-1) * $item_per_page);

      $products = $product_result->skip($skiper)->take($item_per_page)->get();
      // $products = $product_result->get();
      // $product_pagination = $product_result->paginate($item_per_page);
      $product_pagination = $this->paginate($item_per_page, $page_current, $total_pages);

      $json = array(
        'products' => $products,
        'pagination' => "" . $product_pagination
      );
      echo json_encode($json);

    }



    public function getSearchPostNa(){
      $search_area = Input::get('search');
      $activity = Input::get('activity');
      $page = Input::get('page');
      $search_area = str_replace("\"","",$search_area);
      $search_result = Product::where(function($q) use ($search_area, $activity){
        $q->orwhere('name_en','like','%'.$search_area.'%');
        $q->orwhere('name_th','like','%'.$search_area.'%');
      });
      $search_result->where('activity',$activity);
      $search_result->whereNotBetween('product_type_id', [1, 9]);
      $products_count = $search_result->count();


      $page_number = $page;
      $item_per_page = 1;
      $page_current = $page;
      $total_pages = ceil($products_count/$item_per_page);
      $skiper = (($page_number-1) * $item_per_page);

      $products = $search_result->skip($skiper)->take($item_per_page)->get();
      $product_pagination = $this->paginate($item_per_page, $page_current, $total_pages);
      $json = array(
        'search_result' => $products,
        'pagination' => "". $product_pagination
      );
      echo  json_encode($json);
    }

    public function paginate($item_per_page, $page_current, $total_pages){
        $pagination='';
        $flaglast = true;
        if($page_current <= $total_pages){
          $pagination .='<ul class="pagination" >';
          $page_current_prev = $page_current-1;
          $page_current_next = $page_current+1;

            if($page_current == 1 ){
              $pagination .='<li class="disabled"><span>«</span></li>';
            }else{
              $pagination .='<li ><a href="http://localhost/thaifullhouse/public/result/result?page='.$page_current_prev.'" rel="prev">«</a></li>';
            }

            for($i = 1; $i<=$total_pages;$i++){
              if($i == $page_current){
                $pagination .='<li class="active"><span>'.$i.'</span></li>';
              }else{
                $pagination .='<li><a href="http://localhost/thaifullhouse/public/result/result?page='.$i.'">'.$i.'</a></li>';
              }
            }

            if($page_current != $total_pages){
              $pagination .='<li ><a href="http://localhost/thaifullhouse/public/result/result?page='.$page_current_next.'" rel="next">»</a></li>';
            }else{
              $pagination .='<li class="disabled"><span>»</span></li>';
            }
        }
        $pagination .='</ul>';
        return $pagination;


    }
}
