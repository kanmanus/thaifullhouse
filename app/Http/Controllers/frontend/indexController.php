<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use App\Models\Project;
use App\Models\Product;
use App\Models\LandDevelopment;

class IndexController extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function __construct(){


    }

    // join('product_category', 'product.product_id', '=', 'product_category.product_id')
    //                   ->where('category_id', $cat_id)
    //                   ->get();
    public function getPremiumProject(){
      $project = Project::
                join( 'land_dev', 'project.land_dev_id', '=', 'land_dev.land_dev_id')
                ->get();

      $product_all = Product::
                // join( 'product_type', 'product.product_type_id', '=', 'product_type.product_type_id')
                where('activity','buy')
                ->orwhere('activity','rent')
                ->orwhere('product_type_id',11)
                ->orwhere('product_type_id',12)
                ->orderBy('updated_at','desc')
                ->get();

      $product_buy = Product::
                          // join( 'product_type', 'product.product_type_id', '=', 'product_type.product_type_id')
                // where('activity','buy')

                orwhere('activity', 'buy', 'product_type_id', 11 , 'product_type_id', 12 )
                // ->orwahere('product_type_id',12)
                ->orderBy('updated_at','desc')
                ->get();

      $product_rent = Product::
                          // join( 'product_type', 'product.product_type_id', '=', 'product_type.product_type_id')
                // where('activity','rent')
                // ->orwhere('product_type_id',11)
                // ->orwhere('product_type_id',12)
                orwhere('activity', 'rent', 'product_type_id', 11 , 'product_type_id', 12 )
                ->orderBy('updated_at','desc')
                ->get();




      return view('frontend.index', compact('project','product_all','product_buy','product_rent'));
    }

}
