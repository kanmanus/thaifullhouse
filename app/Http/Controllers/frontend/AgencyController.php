<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use App\Models\Agency;
use App\Models\Reviews;


class AgencyController extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function __construct(){


    }

    // join('product_category', 'product.product_id', '=', 'product_category.product_id')
    //                   ->where('category_id', $cat_id)
    //                   ->get();
    public function getAgencys(){
      $agencys = Agency::all();
                // join( 'reviews', 'agency.agency_id', '=', 'reviews.agency_id')
                // ->get();
      foreach($agencys as $agency_reviews) {
          $reviews = Reviews::
                      where('agency_id',$agency_reviews->agency_id)->get();
          $agency_reviews -> reviews = $reviews;
      }
      // $star = Reviews::
                // where('agency_id',$agencyid)->count();

      // $product_buy = Product::
      //                     // join( 'product_type', 'product.product_type_id', '=', 'product_type.product_type_id')
      //           // where('activity','buy')
      //
      //           orwhere('activity', 'buy', 'product_type_id', 11 , 'product_type_id', 12 )
      //           // ->orwahere('product_type_id',12)
      //           ->orderBy('updated_at','desc')
      //           ->get();
      //

      return view('frontend.agency', compact('agencys'));
    }

    public function getAgencyDetail($agencyid){
      $agency_detail = Agency::
                where('agency.agency_id',$agencyid)->get();

      foreach($agency_detail as $agency_reviews) {
            $reviews = Reviews::
                      where('agency_id',$agency_reviews->agency_id)->get();
            $agency_reviews -> reviews = $reviews;
      }
                // join( 'reviews', 'agency.agency_id', '=', 'reviews.agency_id')
                // ->where('agency.agency_id',$agencyid)
                // ->get();

      $review = Reviews::
                where('agency_id',$agencyid)->get();

      $star = Reviews::
                where('agency_id',$agencyid)->count();



      return view('frontend.agency-detail', compact('agency_detail', 'review', 'star'));
    }

}
