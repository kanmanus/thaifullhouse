<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
  'as' => 'home',
  // function () {
  //   return view('frontend.index');
  // }
  'uses' => 'Frontend\indexController@getPremiumProject'
]);

// Route::get('result/{activity}', [
//   'as' => 'result',
//   // function () {
//   //   return view('frontend.result');
//   // }
//   // 'uses' => 'Frontend\ResultController@getResult'
//   'uses' => 'Frontend\ResultController@getResults'
// ]);

Route::get('result/{activity_name}', [
  'as' => 'result',
  'uses' => 'Frontend\ResultController@getResults'
]);

Route::post('result/{activity_name}', [
  'as' => 'result',
  'uses' => 'Frontend\ResultController@getResultsPost'
]);

// Route::post('result-search', [
//   'as' => 'result-search',
//   'uses' => 'Frontend\ResultController@getResultsPost'
// ]);

Route::post('/search-live', [
  'as' => 'search-live',
  'uses' => 'Frontend\ResultController@getSearchPostNa'
]);

Route::post('/search-select', [
  'as' => 'search-select',
  'uses' => 'Frontend\ResultController@getResultsPost'
]);

Route::get('/result-detail', [
  'as' => 'result-detail',
  function () {
    return view('frontend.result-detail');
  }

]);

Route::get('/token', function(){
  return csrf_token();
});

Route::get('/agency', [
  'as' => 'agency',
  'uses' => 'Frontend\AgencyController@getAgencys'
]);

Route::get('agency-detail/{agencyid}', [
  'as' => 'agency-detail',
  'uses' => 'Frontend\AgencyController@getAgencyDetail'
]);

Route::get('/services', [
    'as' => 'services',
    function () {
      return view('frontend.services');
    }
]);

Route::get('/service-detail', [
    'as' => 'service-detail',
    function () {
      return view('frontend.service-detail');
    }
]);

Route::get('/service-detail-2', [
    'as' => 'service-detail-2',
    function () {
      return view('frontend.service-detail-2');
    }
]);

Route::get('/new-properties', [
    'as' => 'new-properties',
    'uses' => 'Frontend\NewPropertiesController@getNewProperties'
]);

/* --------------------- Admin ------------------- */
Route::group(['prefix' => '_admin'], function () {
    Route::controller('login', 'Backend\LoginController');
    Route::get('logout', 'Backend\LoginController@logout');
});
Route::group(['middleware'=>'admin','prefix' => '_admin'], function () {
    Route::get('/', function () { return view('backend.index'); });
    Route::resource('user-management','Backend\AdminController');
    Route::resource('role','Backend\AdminRoleController');
    Route::resource('product_type','Backend\ProductTypeController');
    Route::resource('product','Backend\ProductController');
    Route::resource('direction','Backend\DirectionController');
    Route::resource('project','Backend\ProjectController');
    Route::resource('user','Backend\UserController');
    Route::resource('page','Backend\AdminPageController');
    Route::resource('LandDevelopment','Backend\LandDevelopmentController');
    Route::resource('agency','Backend\AgencyController');
    Route::resource('reviews','Backend\ReviewsController');
    Route::post('check-username','Backend\CheckUsernameController@checkuser');
});

/* --------------------- Theme ------------------- */
Route::group(['prefix' => '_theme'], function () {
    Route::get('blank',function(){
        return view('backend.theme_component.blank');
    });
    Route::get('form',function(){
        return view('backend.theme_component.form');
    });
    Route::get('list',function(){
        return view('backend.theme_component.list');
    });
});
