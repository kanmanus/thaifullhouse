<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LandDevelopment extends Model
{
    use SoftDeletes;
    public $table = 'land_dev';
    public $primaryKey = 'land_dev_id';
    public $fillable = ['name','logo'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
