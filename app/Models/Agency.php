<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agency extends Model
{
    use SoftDeletes;
    public $table = 'agency';
    public $primaryKey = 'agency_id';
    public $fillable = ['agency_name_th','agency_name_en','agency_call','agency_picture','suggest','home_sold','description','experience'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
