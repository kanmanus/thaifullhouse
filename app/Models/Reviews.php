<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reviews extends Model
{
    use SoftDeletes;
    public $table = 'reviews';
    public $primaryKey = 'review_id';
    public $fillable = ['customer_name_en','customer_name_th','customer_title_en','customer_title_th','description_en','description_th','star'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
