<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Direction extends Model
{
    use SoftDeletes;
    public $table = 'direction';
    public $primaryKey = 'direction_id';
    public $fillable = ['name','lat','lng'];

    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
