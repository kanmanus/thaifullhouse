<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    public $table = 'product';
    public $primaryKey = 'product_id';
    public $fillable = ['product_type_id','lat','lng','name_th','name_en','activity','price','per_period','description','bed','bath','level','area','area_unit','status'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
