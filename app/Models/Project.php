<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;
    public $table = 'project';
    public $primaryKey = 'project_id';
    public $fillable = ['name','land_dev_id','banner'];
    protected $guarded = [];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
