@extends('frontend.layout.main-layout')

@section('title', ' - Home')

@section('css')
  {!! Html::style('css/frontend/home.css') !!}

@endsection

@section('content')
<div class="container-fluid search-bar search-bar-map">
  <form id="search-form">
    <div class="row">
      <div class="col-md-12 col-lg-6 col-xs-12 search-form">
        <div class="form-group-search inner-addon left-addon" id="test-search-na">
          <input type="text" class="form-control" id="search-naja" placeholder="Search">
          <div id="suggesstion-box"></div>
          <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
        </div>
      </div>
      <div class="col-md-2 col-lg-1 col-sm-2 col-xs-12 filter-menu filter-menu-first">
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle btn-filter" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <p>Price Range</p>
            <span class="filter-all">All</span>
          </button>
          <span class="caret"></span>
          <ul class="dropdown-menu price-range" aria-labelledby="dropdownMenu1">
            <li>
              <input type="text" id="price-min" placeholder="Min">
              -
              <input type="text" id="price-max" placeholder="Max">
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-2 col-lg-1 col-sm-2 col-xs-12 filter-menu">
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle btn-filter" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              <p>Property Type</p>
              <span class="filter-all">All</span>
          </button>
          <span class="caret"></span>
          <ul class="dropdown-menu property-type" aria-labelledby="dropdownMenu2">
            <li>
              <div class="checkbox">
                <label><input type="checkbox" id="apartment" name="property[]" value="17" checked="checked">Apartment</label>
              </div>
            </li>
            <li>
              <div class="checkbox">
                <label><input type="checkbox" id="condos" name="property[]" value="11" checked="checked">Condos</label>
              </div>
            </li>
            <li>
              <div class="checkbox">
                <label><input type="checkbox" id="commercial" name="property[]" value="15" checked="checked">Commercial Building</label>
              </div>
            </li>
            <li>
              <div class="checkbox">
                <label><input type="checkbox" id="town-house" name="property[]" value="14" checked="checked">Town House</label>
              </div>
            </li>
            <li>
              <div class="checkbox">
                <label><input type="checkbox" id="single-house" name="property[]" value="12" checked="checked">Single House</label>
              </div>
            </li>
            <li>
              <div class="checkbox">
                <label><input type="checkbox" id="lands" name="property[]" value="16" checked="checked">Lands</label>
              </div>
            </li>
          </ul>
        </div>
      </div>

      <div class="col-md-2 col-lg-1 col-sm-2 col-xs-12 filter-menu">
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle btn-filter" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <p>Listing Type</p>
            <span class="filter-all">All</span>
          </button>
          <span class="caret"></span>
          <ul class="dropdown-menu listing-type" aria-labelledby="dropdownMenu3">
            <li>
              <div class="checkbox">
                <label><input type="checkbox" id="for-sales" name="list[]" value="sale">For Sales By Owner</label>
              </div>
            </li>
            <li>
              <div class="checkbox">
                <label><input type="checkbox" id="multiple-service" name="list[]" value="multi">Multiple Listing Services</label>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-2 col-lg-1 col-sm-2 col-xs-12 filter-menu">
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle btn-filter" type="button" id="dropdownMenu4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <p>Bed & Bath</p>
            <span class="filter-all">All</span>
          </button>
          <span class="caret"></span>
          <ul class="dropdown-menu bed-bath" aria-labelledby="dropdownMenu4">
            <h5 class="bed-title">Bedrooms</h5>
            <li class="any-rad">
              <input type="radio" id="any-beds" name="bedrooms" value="0" checked="checked" />
              <label for="any-beds">Any</label>
            </li>
            <li>
              <input type="radio" id="one-bed" name="bedrooms" value="1"/>
              <label for="one-bed">1+</label>
            </li>
            <li>
              <input type="radio" id="two-beds" name="bedrooms" value="2"/>
              <label for="two-beds">2+</label>
            </li>
            <li>
              <input type="radio" id="three-beds" name="bedrooms" value="3"/>
              <label for="three-beds">3+</label>
            </li>
            <li>
              <input type="radio" id="other-beds" name="bedrooms" value="4"/>
              <label for="other-beds">4+</label>
            </li>
            <h5 class="bath-title">Bathrooms</h5>
            <li class="any-rad">
              <input type="radio" id="any-bath" name="bathrooms" value="0" checked="checked" />
              <label for="any-bath">Any</label>
            </li>
            <li>
              <input type="radio" id="one-bath" name="bathrooms" value="1"/>
              <label for="one-bath">1+</label>
            </li>
            <li>
              <input type="radio" id="two-baths" name="bathrooms" value="2" />
              <label for="two-baths">2+</label>
            </li>
            <li>
              <input type="radio" id="three-baths" name="bathrooms" value="3"/>
              <label for="three-baths">3+</label>
            </li>
            <li>
              <input type="radio" id="other-baths" name="bathrooms" value="4" />
              <label for="other-baths">4+</label>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-2 col-lg-1 col-sm-2 col-xs-12 filter-menu">
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle btn-filter" type="button" id="dropdownMenu5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <p>More Filter</p>
          </button>
          <span class="caret"></span>
          <ul class="dropdown-menu more-filter" aria-labelledby="dropdownMenu5">
            <li><a href="#">Action</a></li>
          </ul>
        </div>
      </div>
    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <?php
            $teet = $_SERVER['REQUEST_URI'];
            $teet = str_replace("/"," ",$teet);
            $pieces = explode(" ", $teet);
            $piece = explode("?", $pieces[4]);
            // echo $pieces[1].'<br>';
            // echo $pieces[0].'<br>';
            echo "<input type='hidden' name='activity' value='$piece[0]'>";
    ?>


  </form>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 row-list-map">
      <!-- <button class="btn pull-left btn-list"><img src="../images/list-icon.png" alt="List">List</button>
      <button class="btn pull-left btn-map"><img src="../images/map-icon.png" alt="Map">Map</button> -->
      <!-- <button class="btn pull-right btn-search-result">Search</button>
      <p class="text-center">285 Result | 3 Filters | <a href="#">Save This Search</a></p> -->
    </div>
  </div>
</div>

<div class="container-fluid container-map">
  <div class="row row-equal-map">
    <div class="col-md-6 col-lg-6 col-sm-3 hidden-xs">
      <div id="map"></div>
    </div>

    <div class="col-md-6 col-lg-6 col-sm-9 col-xs-12 container-result">
      <div class="result-wrap-title clearfix">
        <p class="result-place pull-left">Bangkok, Bkk Homes For Sale & Real Estate</p>
        <!-- <p class="result-place pull-left">8,399 Bangkok, Bkk Homes For Sale & Real Estate</p> -->
        <div class="result-btn-group">
          <button class="btn btn-pic pull-right active"><img src="../images/map-icon.png" alt="Map">Pic</button>
          <button class="btn btn-result-list pull-right"><img src="../images/list-icon.png" alt="List">List</button>
        </div>
      </div>

      <div class="detail-list col-md-12 col-sm-12 col-xs-12" id="result-pic">
          @foreach($product_result as $content)
          <?php
          $data[] = array('lat'=>$content['lat'], 'lng'=>$content['lng']);
          $jsonLatLng = json_encode($data);
          ?>
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 result-item">
            <a href="{{ URL::route('result-detail') }}">
              <img src="../images/TestPhoto.png" alt="test" class="img-responsive">
              <div class="carousel-caption caption-detail">
                <div class="clearfix">
                  <h5 class="text-left">{{ strtoupper($content['activity']) }}</h5>
                  <h4 class="text-left">{{ $content['price'] }} THB/month</h4>
                </div>
                <p class="address">Sukhumvit, Bangkok</p>
                <p class="detail">{{ $content['bed'] }} Beds | {{ $content['bath'] }} Baths </p>
                <img class="wishlish-icon" src="../images/icon/ชื่นชอบ.png" alt="">
              </div>
            </a>
          </div>

          @endforeach
          <!-- {!! $product_result->render() !!} -->

<?php
    if(!empty($jsonLatLng)){
      $jsonLatLng = str_replace("\""," ",$jsonLatLng);
      // $json = str_replace(" ","",$json);
      $count_result  = count($product_result);
      echo '<script type="text/javascript">';
      echo "var data = $jsonLatLng;"; // ส่งค่า $data จาก PHP ไปยังตัวแปร data ของ Javascript
      echo "var count = '$count_result';";
      echo '</script>';
    }else{
      echo '<h2 class="text-center">No Result</h2>';
    }

?>


    </div>

    <div class="detail-list col-md-12 col-sm-12 col-xs-12 hidden" id="result-list">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <table class="table table-hover table-responsive result-list-table">
          <thead class="result-list-head">
            <tr>
              <th>Source</th>
              <th>Source</th>
              <th>Address</th>
              <th>City</th>
              <th>Price</th>
              <th>Bed</th>
              <th>Bath</th>
              <th>Sq.Ft.</th>
            </tr>
          </thead>

          <tbody class="result-list-body">
            @foreach($product_result as $content)
            <tr>
              <td class="first-source">{{ $content['type'] }}</td>
              <td class="second-source">{{ $content['activity'] }}</td>
              <td>
                <div class="result-list-address">
                  <p>2513 Popkins Ln,</p>
                  <span class="address-min">Alexandia, VA 22306</span>
                </div>
              </td>
              <td class="city">Naperville</td>
              <td>${{$content['price']}}</td>
              <td>{{$content['bed']}}</td>
              <td>{{$content['bath']}}</td>
              <td>2,200</td>
            </tr>
            @endforeach
          </tbody>


        </table>
      </div>
    </div>
    <div id="pagination" class="col-md-6 col-lg-6 col-sm-6 col-xs-12" active>
    {!! $product_result->render() !!}
    </div>
    <div id="pagination-live" class="col-md-6 col-lg-6 col-sm-6 col-xs-12" hidden>
    {!! $product_result->render() !!}
    </div>
  </div>
</div>

<div class="navbar navbar-default footer">
  <div class="container-fluid">
    <p class="navbar-text pull-left">
      This Coop located at 79 Hamilton Pl, New York NY, 10031 is currently for sale and has been listed on Trulia for 1251 days. This property is listed by Willie Kathryn Suggs Licensed Real Estate Bro for $499,999. 79 Hamilton Pl #23 has 3 beds and 1 bath. 79 Hamilton Pl is in the Hamilton Heights neighborhood in New York and in ZIP Code 10031. The list price of $499,999 is 54% lower
      than the average list price of $1,083,181 for Hamilton Heights real estate, 57% lower than the average list price of $1,155,947 for 10031 real estate, and 86% lower than the average list price of $3,566,722 for New York, NY real estate. The price for this property is 31% lower than the average sales price of $726,807 for Hamilton Heights, 36% lower than the average sales price of $786,291
      for 10031, and 66% lower than the average sales price of $1,492,392 for New York.
    </p>
  </div>
</div>

@endsection

@section('script')
<script>
var infoWindow;
var markers = [];
var details = [];

@foreach($product_result as $key => $test)
var detailHouse1 = '<div class="iw-container" data-index={{$key}}>'+
  '<div class="iw-title">'+
  '<a href="index.html">' +
  '{{$test['price']}}' +
  '</a>' +
  '</div>'+
  '</div>';
details.push(detailHouse1);
@endforeach

var imageDatas = [];
@foreach($product_result as $imageDatas)
var imageData1 = '<div class="iw-img-container row">'+
  '<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">' +
  '<a href="#">' +
  '<div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">' +
  '<img src="../images/TestPhoto.png" class="img-responsive">' +
  '</div>' +
  '<div class="col-md-6 col-sm-6 col-xs-6 col-lg-6 iw-detail">' +
  '<h5>${{ $imageDatas['price'] }}</h5>' +
  '<p>Studio</p>' +
  '<p class="address">{{ $imageDatas['description'] }}</p>' +
  '</div>' +
  '</a>' +
  '</div>'+
  '</div>';
imageDatas.push(imageData1);

@endforeach
var latAndLng = data;


  $("input[name='property[]']").on('click', function(e){
    submitForm();
  });

  $("input[name='list[]']").on('click', function(e){
    submitForm();
  });

  $("input[name='bedrooms']").on('click', function(e){
    submitForm();
  });

  $("input[name='bathrooms']").on('click', function(e){
    submitForm();
  });

  $(document).on('click', '#pagination .pagination a', function (e) {
    e.preventDefault();
    submitForm($(this).attr('href').split('page=')[1]);
    // alert($(this).attr('href').split('page=')[1]);

  });

  $(document).on('click', '#pagination-live .pagination a', function (e) {
    e.preventDefault();
    var keyup = $("#search-naja").val();
    var page = $(this).attr('href').split('page=')[1];
    liveSearch(keyup, page);
  });

  $("#search-naja").keyup(function(){
    var keyup  = $(this).val();
    liveSearch(keyup);
  });

function liveSearch(key, page){

  var pathname = window.location.pathname;
  var res = pathname.split("/");
  if(page == null)
      page = 1;

  $.ajax({
    url: '{{ URL::to('search-live') }}',
    type: 'post',
    // data: {search : $(this).val(), activity: res[4], page: page, _token: '{{ csrf_token() }}'},
    data: {search : key, activity: res[4], page: page, _token: '{{ csrf_token() }}'},
    dataType: 'json',
    success: function(data){
      var array_search_result = data.search_result;
      var array_pagination = data.pagination;
      var list_search = [];
      for(index=0; index<array_search_result.length; index++){
        var data_search = data.search_result[index];
        var data_pic = '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 result-item">'+
          '<a href="{{ URL::route('result-detail') }}">'+
            '<img src="../images/TestPhoto.png" alt="test" class="img-responsive">'+
            '<div class="carousel-caption caption-detail">'+
              '<div class="clearfix">'+
                '<h5 class="text-left"></h5>'+
                // '<h4 class="text-left">'+ data[index].price +'THB/month</h4>'+
                '<h4 class="text-left">'+ data_search.price +'THB/month</h4>'+
              '</div>'+
              '<p class="address">Sukhumvit, Bangkok</p>'+
              // '<p class="detail">'+ data[index].bed + ' Beds | '+ data[index].bath +' Baths </p>'+
              '<p class="detail">'+ data_search.bed + ' Beds | '+ data_search.bath +' Baths </p>'+
              '<img class="wishlish-icon" src="../images/icon/ชื่นชอบ.png" alt="">'+
            '</div>'+
          '</a>'+
        '</div>';
        list_search.push(data_pic);
      }
      if( array_search_result.length == 0 ){
        $("#result-pic").html("Not Result");
        $("#pagination").hide();
        $("#pagination-live").hide();
      }else{
        $("#result-pic").html(list_search);
        $("#pagination-live").html(array_pagination);
        $("#pagination").hide();
        $("#pagination-live").show();
      }

    }
  });
}

function submitForm($page){
  if($page == null){
    $page = 1;
  }
  console.log($('#search-form').serialize());
  $.ajax({
    // url: 'result',
    url: '{{ URL::to('search-select') }}',
    type: 'post',
    data: $('#search-form').serialize()+'&page='+$page,
    dataType: 'json',
    success: function(datas){
      // console.log(datas);
      var listDetails = [];
      var picDetails = [];
      var objLatLng =[];
      details = [];
      imageDatas = [];
      detailHouse1 ='';
      imageData1 ='';

      console.log(datas.products);
      console.log(datas.pagination);
       var arrayProduct = datas.products;
       var pagination = datas.pagination;

      for(index = 0;index < arrayProduct.length; index++){
        var arrayProductDetail = datas.products[index];
        var data_pic = '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 result-item">'+
          '<a href="{{ URL::route('result-detail') }}">'+
            '<img src="../images/TestPhoto.png" alt="test" class="img-responsive">'+
            '<div class="carousel-caption caption-detail">'+
              '<div class="clearfix">'+
                '<h5 class="text-left"></h5>'+
                '<h4 class="text-left">'+ arrayProductDetail.price +'THB/month</h4>'+
              '</div>'+
              '<p class="address">Sukhumvit, Bangkok</p>'+
              '<p class="detail">'+ arrayProductDetail.bed + ' Beds | '+ arrayProductDetail.bath +' Baths </p>'+
              '<img class="wishlish-icon" src="../images/icon/ชื่นชอบ.png" alt="">'+
            '</div>'+
          '</a>'+
        '</div>';

       var data_list ='<tr>'+
            '<td class="first-source">'+ arrayProductDetail.type +'</td>'+
            '<td class="second-source">'+ arrayProductDetail.activity +'</td>'+
            '<td>'+
              '<div class="result-list-address">'+
                '<p>2513 Popkins Ln,</p>'+
                '<span class="address-min">Alexandia, VA 22306</span>'+
              '</div>'+
            '</td>'+
            '<td class="city">Naperville</td>'+
            '<td>$'+ arrayProductDetail.price +'</td>'+
            '<td>'+ arrayProductDetail.bed +'</td>'+
            '<td>'+ arrayProductDetail.bath +'</td>'+
            '<td>2,200</td>'+
          '</tr>';

         detailHouse1 = '<div class="iw-container" data-index='+'>'+
          '<div class="iw-title">'+
          '<a href="index.html">' +
           arrayProductDetail.price+
          '</a>' +
          '</div>'+
          '</div>';
         imageData1 = '<div class="iw-img-container row">'+
          '<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">' +
          '<a href="#">' +
          '<div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">' +
          '<img src="../images/TestPhoto.png" class="img-responsive">' +
          '</div>' +
          '<div class="col-md-6 col-sm-6 col-xs-6 col-lg-6 iw-detail">' +
          '<h5>'+ arrayProductDetail.price +'</h5>' +
          '<p>Studio</p>' +
          '<p class="address">'+ arrayProductDetail.description +'</p>' +
          '</div>' +
          '</a>' +
          '</div>'+
          '</div>';
          var jsonStr = '{lat: '+arrayProductDetail.lat+ ', lng: '+arrayProductDetail.lng+'}';

          listDetails.push(data_list);
          picDetails.push(data_pic);
          details.push(detailHouse1);
          imageDatas.push(imageData1);
          objLatLng.push(jsonStr);

      }
      var arrayLatLng = JSON.stringify(objLatLng);
      $("#result-pic").html(picDetails);
      $(".result-list-body").html("");
      $(".result-list-body").html(listDetails);
      $("#pagination").html("");
      $("#pagination").html(pagination);
      $("#pagination").show();
      $("#pagination-live").hide();


      // $("#map").html("");
      // initMaps();
      // $("#map").html(details);
      // $("#result-pic").html(JSON.stringify(laphom));

      // var map = new google.maps.Map(document.getElementById('map'), {
      //   zoom: 7,
      //   center: {lat: -22.363, lng: 131.044}
      // });
      //
      // loopAddMarker(arrayLatLng);
      // setMapOnAll(map);
      //
      // google.maps.event.addListener(infoWindow, 'domready', function() {
      //   changeStyleInfoWindow(map);
      // });
      // alert(datas);
      // $("#result-pic").html(datas);
    }
  });
}
</script>

{!! Html::script('./js/frontend/app.js') !!}

<!-- <script src="https://maps.googleapis.com/maps/api/js?callback=initMaps" async defer></script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3BdfLideGNs8s3920WW8z9k2lP3fyjh8&callback=initMaps" async defer></script>

@endsection
