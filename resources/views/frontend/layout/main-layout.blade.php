<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>{{ Config::get('constants.APP_NAME') }}@yield('title')</title>

    <!-- Responsive Window -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Custom fonts -->
    {!! Html::style('https://fonts.googleapis.com/css?family=Open+Sans:400,300,700') !!}

    <!-- CSS Files -->
    {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('css/frontend/main_1.css') !!}
    @yield('css')
  </head>

  <body>
    @include('frontend.include.header')

    <div id="ui-main">
      @yield('content')
    </div>

    @include('frontend.include.footer')

    <!-- {!! Html::script('assets/global/jquery-2.1.0.min.js') !!} -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
    
    @yield('script')
  </body>
</html>
