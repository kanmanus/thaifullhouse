@extends('frontend.layout.main-layout')

@section('title', ' - Home')

@section('css')
  {!! Html::style('css/frontend/home.css') !!}
  {!! Html::style('bxslide/jquery.bxslider.css') !!}
@endsection

@section('content')
<div class="container-fluid section-new-properties">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>
          New Properties
        </h1>
        <div class="clearfix span-group">
          <div class="buy-rent-all pull-right">
            <span class="all-ppt active">All</span>
            <span id="buy-ppt">Buy</span>
            <span id="rent-ppt">Rent</span>
          </div>
        </div>
        <hr>
      </div>


      <div class="detail-list col-md-12 col-sm-12 col-xs-12" id="result-pic-1">
        @foreach($product_all as $key => $productAll)

        <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 result-item">
          <a href="{{ URL::route('result-detail')}}">
            <img src="images/TestPhoto.png" alt="test" class="img-responsive">
            <div class="carousel-caption caption-detail">
              <div class="clearfix">
                <h5 class="text-left">{{ $productAll->activity }}</h5>
                <h4 class="text-left">{{ $productAll->price }} THB/month</h4>
              </div>
              <p class="address">Sukhumvit, Bangkok</p>
              <p class="detail">{{ $productAll->bed }} Beds | {{ $productAll->bath }} Baths </p>
              <img class="wishlish-icon" src="images/icon/ชื่นชอบ.png" alt="">
            </div>
          </a>
        </div>
        
        @endforeach
      </div>

      <div class="detail-list col-md-12 col-sm-12 col-xs-12" id="result-pic-2" style="display:none;">
        @foreach($product_buy as $key => $productBuy)

            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 result-item">
              <a href="{{ URL::route('result-detail')}}">
                <img src="images/TestPhoto.png" alt="test" class="img-responsive">
                <div class="carousel-caption caption-detail">
                  <div class="clearfix">
                    <h5 class="text-left">{{ $productBuy->activity }}</h5>
                    <h4 class="text-left">{{ $productBuy->price }} THB/month</h4>
                  </div>
                  <p class="address">Sukhumvit, Bangkok</p>
                  <p class="detail">{{ $productBuy->bed }} Beds | {{ $productBuy->bath }} Baths </p>
                  <img class="wishlish-icon" src="images/icon/ชื่นชอบ.png" alt="">
                </div>
              </a>
            </div>

        @endforeach
      </div>

      <div class="detail-list col-md-12 col-sm-12 col-xs-12" id="result-pic-3" style="display:none;">
        @foreach($product_rent as $key => $productRent)

            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 result-item">
              <a href="{{ URL::route('result-detail')}}">
                <img src="images/TestPhoto.png" alt="test" class="img-responsive">
                <div class="carousel-caption caption-detail">
                  <div class="clearfix">
                    <h5 class="text-left">{{ $productRent->activity }}</h5>
                    <h4 class="text-left">{{ $productRent->price }} THB/month</h4>
                  </div>
                  <p class="address">Sukhumvit, Bangkok</p>
                  <p class="detail">{{ $productRent->bed }} Beds | {{ $productRent->bath }} Baths </p>
                  <img class="wishlish-icon" src="images/icon/ชื่นชอบ.png" alt="">
                </div>
              </a>
            </div>

        @endforeach
      </div>
    </div>
  </div>
</div>


@endsection

@section('script')
<script>
  $(document).ready(function(){
    $(".slide1").on("click",function(e){
      e.preventDefault(); // Not event

      banner = $(this).data('banner');
      $('#banner-head').attr('src', banner);
      // $('#banner-head').attr('src',"images/logo/logo-5.png");
    });
    $(".all-ppt").click(function(){
      $("#result-pic-1").show();
      $("#result-pic-2").hide();
      $("#result-pic-3").hide();

    });
    $("#buy-ppt").click(function(){
      $("#result-pic-1").hide();
      $("#result-pic-2").show();
      $("#result-pic-3").hide();

    });
    $("#rent-ppt").click(function(){
      $("#result-pic-1").hide();
      $("#result-pic-2").hide();
      $("#result-pic-3").show();

    });
  });
  $(document).ready(function(){
  // $('.bxslider').bxSlider();
    $('.slider3').bxSlider({
      slideWidth: 200,
      minSlides: 6,
      maxSlides: 6,
      moveSlides: 6,
      slideMargin: 20
    });
  });


  </script>
{!! Html::script('bxslide/jquery.bxslider.min.js') !!}

@endsection
