@extends('frontend.layout.main-layout')

@section('title', ' - Home')

@section('css')
  {!! Html::style('css/frontend/home.css') !!}
@endsection

@section('content')
<div class="container-fluid search-bar">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 search-form">
        <form class="form-horizontal" role="search">
          <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 search-item">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="City, or zip">
            </div>
          </div>
          <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 search-item">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Agent Name, Company">
            </div>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12 search-item">
            <div class="form-group">
              <select class="form-control">
                <option>Serviced Needed</option>
              </select>
            </div>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12 search-item">
            <div class="form-group">
              <select class="form-control">
                <option>Sorted By : Best Match</option>
              </select>
            </div>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
            <div class="form-group">
              <button class="btn btn-search">Search</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row row-equal-agent-height section-agent-info">
    @foreach( $agency_detail as $detail)
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="col-lg-4 col-md-4 col-sm-6">
        <img src="../images/agent.png" alt="" class="img-responsive width-one-hundred">
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 agent-name-review">
        <h1>{{ $detail['agency_name_en'] }} </h1>
        <h4>{{ $detail['agency_call'] }}</h4>
        <div class="agent-review clearfix">
          <!-- <h4 class="pull-left">5.0</h4> -->
              <?php
                $sum = 0;
                $current_score = 0;
                foreach ($detail['reviews'] as $review_star) {
                  $current_score = $review_star['star'];
                  $sum = $current_score + $sum;
                }

                $sum = ($sum / $star);
                $sum = floor($sum);
                echo '<h4 class="pull-left">',$sum,'.0</h4>';
                for($i=0;$i<5;$i++){
                  if( $i < $sum ){
                  echo '<img src="../images/Gold-Star.png" alt="" class="pull-left img-responsive">';
                  }else{
                  echo '<img src="../images/Black-Star.png" alt="" class="pull-left img-responsive">';
                  }

                }
              ?>
        </div>
        <hr>
        <p>Home Sold: {{ $detail['home_sold'] }}</p>
        <p>{{ $detail['description'] }}</p>
      </div>
      <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 agent-contact-form">
        <h4>CONTACT AGENT</h4>
        <form role="form">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Name">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Phone">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Email">
          </div>
          <div class="form-group">
            <textarea class="form-control" rows="4"></textarea>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-submit-message">Send Message</button>
          </div>
        </form>
      </div>
    </div>

  </div>
</div>

<div class="container-fluid section-agent-detail">
  <div class="container">
    <div class="row section-agent-experience">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <!-- <h1>Experience</h1>
        <hr>
        <ul>
          <li>Specializing in Condos. Townhomes and Signle Family Residences</li>
          <li>10 years of real estate experience</li>
        </ul> -->
        {!! $detail['experience']  !!}
      </div>
    </div>

    <div class="row section-agent-reviews">

        <h1>Reviews</h1>
        <hr>
        @foreach( $review as $review_detail )
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 person-info-reviews">
          <h4>{{ $review_detail['customer_name_en']}}</h4>
          <ul>
            <li>{{ $review_detail['customer_title_en'] }}</li>
            <li>$207k</li>
            <li>03/22/16</li>



            <li>
              @if(!empty($review_detail['star']))
                
                   <?php
                        for($i=0;$i < 5;$i++){

                          if(floor($review_detail['star'])>$i){
                            echo '<img src="../images/Gold-Star.png" alt="" class="pull-left img-responsive">';
                            }else {
                            echo '<img src="../images/Black-Star.png" alt="" class="pull-left img-responsive">';
                            }
                        }
                   ?>


              @endif

              <!-- <img src="../images/Gold-Star.png" alt="" class="pull-left img-responsive">
              <img src="../images/Gold-Star.png" alt="" class="pull-left img-responsive">
              <img src="../images/Gold-Star.png" alt="" class="pull-left img-responsive">
              <img src="../images/Black-Star.png" alt="" class="pull-left img-responsive"> -->
            </li>
          </ul>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 person-comment-reviews">
          <!-- <p>Lana Grimaldi recently helped my wife and I purchase our home and she did and amazing job of walking us through the process. She was willing to go to showings at the times that we had available and was very thorough in pointing out key details at each house we looked at. She was quick to respond to our questions and was a calming presence as we dealt with a few stressful moments along the way. We highly recommend Lana and honestly can't say enough good things about what a pleasure it was to do business with her.</p> -->
          <p>{{ $review_detail['description_en'] }}</p>
        </div>

      </div>
      @endforeach
    </div>

    <div class="row section-agent-listing">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h1>Listings</h1>
        <hr>

        <div class="carousel slide multi-item-carousel" id="theCarousel">
          <div class="carousel-inner">
            <div class="item active">
              <div class="col-xs-4"><a href="#1"><img src="../images/agent-house.png" class="img-responsive"></a></div>
            </div>
            <div class="item">
              <div class="col-xs-4"><a href="#1"><img src="../images/agent-house.png" class="img-responsive"></a></div>
            </div>
            <div class="item">
              <div class="col-xs-4"><a href="#1"><img src="../images/agent-house.png" class="img-responsive"></a></div>
            </div>
            <div class="item">
              <div class="col-xs-4"><a href="#1"><img src="../images/agent-house.png" class="img-responsive"></a></div>
            </div>
            <div class="item">
              <div class="col-xs-4"><a href="#1"><img src="../images/agent-house.png" class="img-responsive"></a></div>
            </div>
          </div>
          <a class="left carousel-control" href="#theCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
          <a class="right carousel-control" href="#theCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
        </div>

      </div>
    </div>
    @endforeach
  </div>
</div>
@endsection

@section('script')

@endsection
