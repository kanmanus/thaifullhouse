<!-- <header>
  <div class="container">
    <a href="{{ URL::route('home') }}" id="logo">BKKSOL</a>

    <nav>
      <ul id="nav">
        <li class="nav-item">About Us</li>
        <li class="nav-item">Products</li>
        <li class="nav-item">Blog</li>
        <li class="nav-item">Contact Us</li>
      </ul>
    </nav>
  </div>
</header> -->
<header>
<!-- <div class="container-fluid section-home-on"> -->
<div class="container-fluid">
<nav class="navbar">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ URL::route('home') }}" id="logo">THAIFULLHOUSE</a>
      <!-- <a class="navbar-brand" href="index.html">THAIFULLHOUSE</a> -->
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a id="result-buy" href="{{ URL::route('result', 'buy')}}">BUY</a></li>
        <li><a href="{{ URL::route('result', 'sell')}}">SELL</a></li>
        <li><a href="{{ URL::route('result', 'rent')}}">RENT</a></li>
        <li><a href="#">MORTGAGE</a></li>
        <li><a href="{{ URL::route('agency')}}">AGENT FINDER</a></li>
        <li><a href="#">ADVICE</a></li>
        <li><a href="{{ URL::route('services')}}">SERVICE</a></li>
        <li><a href="#">HOME IDEA</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">EN</a></li>
        <li class="hidden-xs navbar__line">|</li>
        <li><a href="#">TH</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">LOG IN</a></li>
        <li class="hidden-xs navbar__line">|</li>
        <li><a href="#">REGISTER</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>
</header>
