<footer>
  <div class="container-fluid footer-full-house">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
          <h4>OUR COMPANY</h4>
          <ul>
            <li><a href="">About Us</a></li>
            <li><a href="">ForSaleByOwner Blog</a></li>
            <li><a href="">Press Room</a></li>
            <li><a href="">Terms and Conditions</a></li>
            <li><a href="">Privacy Policy</a></li>
            <li><a href="">Abuse and Spam</a></li>
          </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
          <h4>WORK WITH US</h4>
          <ul>
            <li><a href="">Become a Pro</a></li>
            <li><a href="">Real Estate Brokers</a></li>
            <li><a href="">Business Development</a></li>
            <li><a href="">Affiliate Programs</a></li>
            <li><a href="">Advertise</a></li>
            <li><a href="">Careers</a></li>
          </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
          <h4>CUSTOMER SUPPORT</h4>
          <ul>
            <li>Mon-Fri. 7AM-7PM CDT</li>
            <li>Sat-Sun. 8AM-6PM CDT</li>
            <li>888-367-7253</li>
            <li><a href="">Frequently Asked Questions</a></li>
            <li><a href="">Make a Suggestion</a></li>
          </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
          <h4>TOOL AND SERVICE</h4>
          <ul>
            <li><a href="">Mortgage</a></li>
            <li><a href="">Help</a></li>
            <li><a href="">FSBO Store</a></li>
            <li><a href="">Real Estate Legal Form</a></li>
            <li><a href="">Home Selling Guarantee</a></li>
            <li><a href="">Search Foreclosures</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>
