@extends('frontend.layout.main-layout')

@section('title', ' - Home')

@section('css')
  <!-- {!! Html::style('css/frontend/home.css') !!} -->
@endsection

@section('content')
<div class="container-fluid search-bar">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 search-form">
        <h2>10900 Bangkok</h2>
        <form class="form-horizontal" role="search">
          <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 search-item">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="City, or zip">
            </div>
          </div>
          <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 search-item">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Agent Name, Company">
            </div>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12 search-item">
            <div class="form-group">
              <select class="form-control">
                <option>Serviced Needed</option>
              </select>
            </div>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12 search-item">
            <div class="form-group">
              <select class="form-control">
                <option>Sorted By : Best Match</option>
              </select>
            </div>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
            <div class="form-group">
              <button class="btn btn-search">Search</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="container section-agent-list">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-xs-12 col-xs-12 wrap-agent">
      @foreach( $agencys as $age)
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 agent-info">
          <img src="images/agent.png" alt="agent" class="img-responsive width-one-hundred">
          <div class="clearfix">
            <div class="recommended">
              {{ $age->suggest }}
            </div>
          </div>
          <div class="agent-detail">
            <div class="pull-left wrap-agent-info">
              <h5>{{ $age['agency_name_en'] }}</h5>
              <div class="pull-left agent-info-phone">
                <div class="clearfix agent-phone">
                  <img src="images/phone-icon.png" class="img-responsive pull-left" alt="">
                  <p>{{ $age['agency_call']}}</p>
                </div>
                <div class="agent-review clearfix">
                  <!-- <h5 class="pull-left">5.0</h5> -->
                  <!-- <img src="images/Gold-Star.png" alt="" class="pull-left img-responsive">
                  <img src="images/Gold-Star.png" alt="" class="pull-left img-responsive">
                  <img src="images/Gold-Star.png" alt="" class="pull-left img-responsive">
                  <img src="images/Gold-Star.png" alt="" class="pull-left img-responsive">
                  <img src="images/Black-Star.png" alt="" class="pull-left img-responsive"> -->
                  <?php
                    $sum = 0;
                    $current_score = 0;
                    $star_count = count($age['reviews']);
                    // echo $star_count;
                    foreach ($age['reviews'] as $review_star) {
                      $current_score = $review_star['star'];
                      $sum = $current_score + $sum;
                    }

                    $sum = ($sum / $star_count);
                    $sum = floor($sum);
                    echo '<h5 class="pull-left">',$sum,'.0</h5>';
                    for($i=0;$i<5;$i++){
                      if( $i < $sum ){
                      echo '<img src="../images/Gold-Star.png" alt="" class="pull-left img-responsive">';
                      }else{
                      echo '<img src="../images/Black-Star.png" alt="" class="pull-left img-responsive">';
                      }

                    }
                  ?>
                </div>
              </div>
            </div>

            <div class="pull-right agent-contact">

              <a href="{{ URL::route('agency-detail' , $age['agency_id']) }}" class="btn btn-agent">CONTACT</a>
              <p>20 Reviews</p>
            </div>
          </div>
        </div>
       @endforeach
    </div>
  </div>
</div>
@endsection

@section('script')

@endsection
