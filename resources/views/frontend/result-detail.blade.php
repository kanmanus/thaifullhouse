@extends('frontend.layout.main-layout')

@section('title', ' - Home')

@section('css')
  {!! Html::style('css/frontend/home.css') !!}
@endsection

@section('content')
<div class="container-fluid result-detail-bar">
  <div class="container">
    <div class="row">
      <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 result-detail-title">
        <h2>Regency at Readington Villas <span class="author-of-house">by Toll Brothers</span></h2>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 result-detail-btn">
        <a class="btn btn-default btn-save">
          <img src="images/icon/save-icon.png" alt="wishlish" class="img-responsive pull-left wishlish-icon-save">
          SAVE
        </a>
        <a class="btn btn-default btn-share">SHARE</a>
      </div>
    </div>
  </div>
</div>

<div class="container section-result-image-info">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <img src="images/slide-detail-1.png" alt="slide-1" class="img-responsive width-one-hundred">
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 result-info-name">
        <p class="font-for-sale">FOR SALE</p>
        <h2>Regency at Readington Villas</h2>
        <p class="second-title">Whitehouse Station, NJ 08889</p>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 result-info-price">
        <p>Price :</p>
        <h2>$690,000</h2>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result-com-bed-bath">
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 community-column">
          <p>New Community</p>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
          <p>2+ Bedrooms</p>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
          <p>2 - 3 Bathrooms</p>
        </div>
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs pin-column">
          <img src="../public/images/icon/ปักหมุด2.png" alt="" style="height: 3em;">
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid section-result-description">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h1>Description</h1>
        <hr>
      </div>

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 description-part">
        <div class="clearfix">
          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
              <h5>Lot Size</h5>
          </div>
          <div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
            <h5><span class="lot-size-detail">6,030 square feet</span></h5>
          </div>
        </div>
        <hr class="line-description" style="margin-top:0.5em">
      </div>

      <div class="col-lg-12 description-part">
        <div class="clearfix">
          <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
              <h5>Feature</h5>
          </div>
          <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
            <h5>Bedrooms</h5>
            <hr class="line-description">
            <ul>
              <li>Bedrooms: 2</li>
            </ul>
          </div>
          <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
            <h5>Bathrooms</h5>
            <hr class="line-description">
            <ul>
              <li>Full Bathrooms: 2</li>
            </ul>
          </div>
          <div class="col-lg-10 col-lg-offset-2 col-md-10 col-md-offset-2 col-sm-12 col-xs-12 kitchen-dining">
            <h5>Kitchen and Dining</h5>
            <hr class="line-description">
            <ul>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 item-list-detail">
                <li>Counter - Tile</li>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 item-list-detail">
                <li>Gas Range/Cooktop</li>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 item-list-detail">
                <li>Range/Oven Free Standing</li>
              </div>
            </ul>
          </div>
          <div class="col-lg-10 col-lg-offset-2 col-md-10 col-md-offset-2 col-sm-12 col-xs-12 other-rooms">
            <h5>Other rooms</h5>
            <hr class="line-description">
            <ul>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 item-list-detail">
                <li>Counter - Tile</li>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 item-list-detail">
                <li>Gas Range/Cooktop</li>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 item-list-detail">
                <li>Range/Oven Free Standing</li>
              </div>
            </ul>
          </div>
          <div class="col-lg-10 col-lg-offset-2 col-md-10 col-md-offset-2 col-sm-12 col-xs-12 interior-features">
            <h5>Interior Features</h5>
            <hr class="line-description">
            <ul>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 item-list-detail">
                <li>Garage Door Opener</li>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 item-list-detail">
                <li>Water Heater Gas</li>
              </div>
            </ul>
          </div>

        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 amenities">
        <h1>Amenities</h1>
        <hr>
        <ul>
          <li><img src="../public/images/icon/facebook.png" alt=""> สระว่ายน้ำ</li>
          <li><img src="../public/images/icon/googleplus.png" alt=""> สวนส่วนกลาง</li>
          <li><img src="../public/images/icon/googleplus.png" alt=""> พื้นที่สันทนาการ</li>
          <li><img src="../public/images/icon/facebook.png" alt=""> เจ้าหน้าที่รักษาความปลอดภัย 24 ชั่วโมง</li>
        </ul>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 contact-info">
        <h1>Contact Info</h1>
        <hr>
        <form role="form">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Name">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Phone">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Email">
          </div>
          <div class="form-group">
            <textarea class="form-control" rows="4"></textarea>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-submit-message">Send</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')

@endsection
