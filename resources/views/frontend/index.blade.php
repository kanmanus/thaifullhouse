@extends('frontend.layout.main-layout')

@section('title', ' - Home')

@section('css')
  {!! Html::style('css/frontend/home.css') !!}
  {!! Html::style('bxslide/jquery.bxslider.css') !!}
@endsection

@section('content')
<div class="container-fluid section-home-one">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
        <div class="for-sell">
          <h1>For Sell</h1>
          <p>Thai Full House markets your home to millions of buyers each month.</p>
          <a href="" class="btn btn-default btn-sell-home">Sell your home</a>
        </div>
      </div>
      <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
        <div class="search-home">
          <h1>Search Home</h1>
          <p>Finding the perfect home just got easier. Search for homes by schools, activities, safety and more.</p>
          <form role="form" method="get" class="search-home-form">
            <div class="form-group">
              <label for="search-sell" id="label-search-sell" class="pull-left">
                FOR SELL
                <hr>
              </label>
              <label for="search-rent" id="label-search-rent" class="pull-left">
                FOR RENT
                <hr class="hidden">
              </label>
              <input type="text" name="search-home" id="search-sell" class="form-control" placeholder="Address, Bangkok Thailand">
              <input type="text" name="search-home" id="search-rent" class="form-control hidden" placeholder="Address, Bangkok Thailand">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-search-home">Search</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container section-how-it-work">
  <div class="row">
    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
      <h1>How it Works</h1>
      <div class="how-it-work-icon">
        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
          <img src="images/find-home-icon.png" alt="" class="img-responsive">
          <h4>Find Your Home</h4>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
          <img src="images/inspection-icon.png" alt="" class="img-responsive">
          <h4>Get Inspection</h4>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
          <img src="images/deal-closed-icon.png" alt="" class="img-responsive">
          <h4>Close a Deal</h4>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
          <img src="images/residential-moving-icon.png" alt="" class="img-responsive">
          <h4>Move into your new home</h4>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid section-explore-area">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Explore Area & Near By Area </h1>
        <div class="col-md-30 col-sm-6 col-xs-12 explore-item">
          <a href="#">
            <img src="images/Sukhumvit.png" alt="" class="img-responsive">
            <div class="caption-explore">
              <h3>SUKHUMWIT</h3>
            </div>
          </a>
        </div>
        <div class="col-md-15 col-sm-6 col-xs-12 explore-item">
          <a href="#">
            <img src="images/Thonglor.png" alt="" class="img-responsive">
            <div class="caption-explore">
              <h3>THONGLOR</h3>
            </div>
          </a>
        </div>
        <div class="col-md-15 col-sm-6 col-xs-12 explore-item">
          <a href="#">
            <img src="images/MRT.png" alt="" class="img-responsive">
            <div class="caption-explore">
              <h3>MRT</h3>
            </div>
          </a>
        </div>
        <div class="col-md-15 col-sm-6 col-xs-12 explore-item">
          <a href="#">
            <img src="images/BRT.png" alt="" class="img-responsive">
            <div class="caption-explore">
              <h3>BRT</h3>
            </div>
          </a>
        </div>
        <div class="col-md-15 col-sm-6 col-xs-12 explore-item">
          <a href="#">
            <img src="images/ladprao.png" alt="" class="img-responsive">
            <div class="caption-explore">
              <h3>LADPRAO</h3>
            </div>
          </a>
        </div>
        <div class="col-md-15 col-sm-6 col-xs-12 explore-item">
          <a href="#">
            <img src="images/silom.png" alt="" class="img-responsive">
            <div class="caption-explore">
              <h3>SILOM</h3>
            </div>
          </a>
        </div>
        <div class="col-md-15 col-sm-6 col-xs-12 explore-item">
          <a href="#">
            <img src="images/sathorn.png" alt="" class="img-responsive">
            <div class="caption-explore">
              <h3>SATHORN</h3>
            </div>
          </a>
        </div>
        <div class="col-md-15 col-sm-6 col-xs-12 explore-item">
          <a href="#">
            <img src="images/bts.png" alt="" class="img-responsive">
            <div class="caption-explore">
              <h3>BTS</h3>
            </div>
          </a>
        </div>
        <div class="col-md-15 col-sm-6 col-xs-12 explore-item">
          <a href="#">
            <img src="images/shopping-mall.png" alt="" class="img-responsive">
            <div class="caption-explore">
              <h3>SHOPPING MALL</h3>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>





<div class="container section-premium">
  <div class="row">
    <div class="col-md-12">
      <h1>Premium Project</h1>
      <img src="images/pr.png" alt="" id="banner-head" class="img-responsive width-one-hundred">
    </div>


    <div class="col-md-12">
      <div class="slider3">
        @foreach( $project as $project_detail)
        <div class="slide col-sm-2"><a href="#x" class="slide1" data-banner="{{ URL::asset('images/logo/' . $project_detail->banner) }}"><img src="{{ URL::asset('images/logo/' . $project_detail->logo) }}" alt="Image" class="img-responsive "></a></div>
        @endforeach
      </div>
    </div>


  </div>
</div>

<div class="container-fluid section-new-properties">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>
          New Properties
        </h1>
        <div class="clearfix span-group">
          <div class="buy-rent-all pull-right">
            <span class="all-ppt active">All</span>
            <span id="buy-ppt">Buy</span>
            <span id="rent-ppt">Rent</span>
          </div>
        </div>
        <hr>
      </div>


      <div class="detail-list col-md-12 col-sm-12 col-xs-12" id="result-pic-1">
        @foreach($product_all as $key => $productAll)
        @if( $key <= '5' )
        <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 result-item">
          <a href="{{ URL::route('result-detail')}}">
            <img src="images/TestPhoto.png" alt="test" class="img-responsive">
            <div class="carousel-caption caption-detail">
              <div class="clearfix">
                <h5 class="text-left">{{ strtoupper($productAll->activity) }}</h5>
                <h4 class="text-left">{{ $productAll->price }} THB/month</h4>
              </div>
              <p class="address">Sukhumvit, Bangkok</p>
              <p class="detail">{{ $productAll->bed }} Beds | {{ $productAll->bath }} Baths </p>
              <img class="wishlish-icon" src="images/icon/ชื่นชอบ.png" alt="">
            </div>
          </a>
        </div>
        @endif
        @endforeach
      </div>

      <div class="detail-list col-md-12 col-sm-12 col-xs-12" id="result-pic-2" style="display:none;">
        @foreach($product_buy as $key => $productBuy)
          @if( $key <= '5' )
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 result-item">
              <a href="{{ URL::route('result-detail')}}">
                <img src="images/TestPhoto.png" alt="test" class="img-responsive">
                <div class="carousel-caption caption-detail">
                  <div class="clearfix">
                    <h5 class="text-left">{{ strtoupper($productBuy->activity) }}</h5>
                    <h4 class="text-left">{{ $productBuy->price }} THB/month</h4>
                  </div>
                  <p class="address">Sukhumvit, Bangkok</p>
                  <p class="detail">{{ $productBuy->bed }} Beds | {{ $productBuy->bath }} Baths </p>
                  <img class="wishlish-icon" src="images/icon/ชื่นชอบ.png" alt="">
                </div>
              </a>
            </div>
          @endif
        @endforeach
      </div>

      <div class="detail-list col-md-12 col-sm-12 col-xs-12" id="result-pic-3" style="display:none;">
        @foreach($product_rent as $key => $productRent)
          @if( $key <= '5' )
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 result-item">
              <a href="{{ URL::route('result-detail')}}">
                <img src="images/TestPhoto.png" alt="test" class="img-responsive">
                <div class="carousel-caption caption-detail">
                  <div class="clearfix">
                    <h5 class="text-left">{{ strtoupper($productRent->activity) }}</h5>
                    <h4 class="text-left">{{ $productRent->price }} THB/month</h4>
                  </div>
                  <p class="address">Sukhumvit, Bangkok</p>
                  <p class="detail">{{ $productRent->bed }} Beds | {{ $productRent->bath }} Baths </p>
                  <img class="wishlish-icon" src="images/icon/ชื่นชอบ.png" alt="">
                </div>
              </a>
            </div>
          @endif
        @endforeach
      </div>
      <div class="col-md-12">
        <a href="{{ URL::route('new-properties') }}" class="btn btn-default btn-see-more" >See More Listing</a>
      </div>
    </div>
  </div>
</div>

<div class="container section-matching-services">
  <div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
      <h1>Matching Services</h1>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 matching-item">
      <a href="#">
        <img src="images/air-condition.png" alt="test" class="img-responsive matching-item-image">
        <div class="matching-icon-text">
          <img src="images/icon/เครื่องปรับอากาศ.png" alt="" class="img-responsive matching-icon">
          <h2 class="text-air">Air Conditioning</h2>
        </div>
      </a>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 matching-item">
      <a href="#">
        <img src="images/cleaning-image.png" alt="test" class="img-responsive matching-item-image">
        <div class="matching-icon-text">
          <img src="images/icon/cleaning.png" alt="" class="img-responsive matching-icon">
          <h2>Cleaning</h2>
        </div>
      </a>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 matching-item">
      <a href="#">
        <img src="images/electrical-image.png" alt="test" class="img-responsive matching-item-image">
        <div class="matching-icon-text">
          <img src="images/icon/electricity.png" alt="" class="img-responsive matching-icon">
          <h2>Electricity</h2>
        </div>
      </a>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 matching-item">
      <a href="#">
        <img src="images/plumber-image.png" alt="test" class="img-responsive matching-item-image">
        <div class="matching-icon-text">
          <img src="images/icon/plumber.png" alt="" class="img-responsive matching-icon">
          <h2>Plumber</h2>
        </div>
      </a>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 matching-item">
      <a href="#">
        <img src="images/moving-image.png" alt="test" class="img-responsive matching-item-image">
        <div class="matching-icon-text">
          <img src="images/icon/moving.png" alt="" class="img-responsive matching-icon">
          <h2>Moving</h2>
        </div>
      </a>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 matching-item">
      <a href="#">
        <img src="images/laundry-image.png" alt="test" class="img-responsive matching-item-image">
        <div class="matching-icon-text">
          <img src="images/icon/เครื่องซักผ้า.png" alt="" class="img-responsive matching-icon">
          <h2>Laundry</h2>
        </div>
      </a>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
  $(document).ready(function(){
    $(".slide1").on("click",function(e){
      e.preventDefault(); // Not event

      banner = $(this).data('banner');
      $('#banner-head').attr('src', banner);
      // $('#banner-head').attr('src',"images/logo/logo-5.png");
    });
    $(".all-ppt").click(function(){
      $("#result-pic-1").show();
      $("#result-pic-2").hide();
      $("#result-pic-3").hide();

    });
    $("#buy-ppt").click(function(){
      $("#result-pic-1").hide();
      $("#result-pic-2").show();
      $("#result-pic-3").hide();

    });
    $("#rent-ppt").click(function(){
      $("#result-pic-1").hide();
      $("#result-pic-2").hide();
      $("#result-pic-3").show();

    });
  });
  $(document).ready(function(){
  // $('.bxslider').bxSlider();
    $('.slider3').bxSlider({
      slideWidth: 200,
      minSlides: 6,
      maxSlides: 6,
      moveSlides: 6,
      slideMargin: 20
    });
  });


  </script>
{!! Html::script('bxslide/jquery.bxslider.min.js') !!}

@endsection
