@extends('frontend.layout.main-layout')

@section('title', ' - Home')

@section('css')
  {!! Html::style('css/frontend/home.css') !!}
@endsection

@section('content')
<div class="container-fluid section-bg-service-detail">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="bg-service-caption">
          <h1>FIND AIRCONDITIONAL SERVICE</h1>
          <h3>REPAIR AND INSTALLATION CONTRACTORS</h3>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container section-service-question">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h1>Air Condition Installation or Service & Repair</h1>
      <form class="form-horizontal">
        <div class="form-group">
          <h2>What do you need done?</h2>
          <div class="radio">
            <label>
              <input type="radio" name="need" id="need1" value="" checked>
              <span>Installation</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="need" id="need2" value="">
              <span>Service only</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="need" id="need3" value="">
              <span>Repair: Won't turn on</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="need" id="need4" value="">
              <span>Repair: Water dripping/leaking(condensation)</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="need" id="need5" value="">
              <span>Repair: Blowing hot air</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="need" id="need6" value="">
              <span>Gas refill</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="need" id="need7" value="">
              <span>Other (please specify in the detail section below)</span>
            </label>
          </div>
          <hr>
        </div>
        <div class="form-group">
          <h2>How many units?</h2>
          <select class="form-control">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select>
          <hr>
        </div>
        <div class="form-group">
          <h2>What horsepower?</h2>
          <hr>
          <div class="radio">
            <label>
              <input type="radio" name="horse_power" id="horse_power1" value="" checked>
              <span>HP</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="horse_power" id="horse_power2" value="">
              <span>1.5 HP</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="horse_power" id="horse_power3" value="">
              <span>2.0 HP</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="horse_power" id="horse_power4" value="">
              <span>3.0 HP</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="horse_power" id="horse_power5" value="">
              <span>Other</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="horse_power" id="horse_power6" value="">
              <span>Don't know</span>
            </label>
          </div>
        </div>
        <div class="form-group">
          <h2>What type of property do you have?</h2>
          <div class="radio">
            <label>
              <input type="radio" name="property" id="property1" value="" checked>
              <span>Single/Double-Storey House</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="property" id="property2" value="">
              <span>Flat/HDB</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="property" id="property3" value="">
              <span>Apartment/Condo</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="property" id="property4" value="">
              <span>Commercial</span>
            </label>
          </div>
          <hr>
        </div>
        <div class="form-group">
          <h2>Will you provide the air con unit(s)?</h2>
          <div class="radio">
            <label>
              <input type="radio" name="air_con" id="air_con1" value="" checked>
              <span>Installation only. I have purchased the air cond myself</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="air_con" id="air_con2" value="">
              <span>Service provider to supply and install</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="air_con" id="air_con3" value="">
              <span>Not applicable</span>
            </label>
          </div>
          <hr>
        </div>
        <div class="form-group">
          <h2>When do you need it?</h2>
          <div class="radio">
            <label>
              <input type="radio" name="air_con" id="air_con1" value="" checked>
              <span>This week</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="air_con" id="air_con2" value="">
              <span>Next week</span>
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="air_con" id="air_con3" value="">
              <span>Within 30 days</span>
            </label>
          </div>
          <hr>
        </div>
        <div class="form-group">
          <h2>Please describe the job in detail (optional)</h2>
          <textarea name="describe" id="describe" class="form-control" rows="6"></textarea>
          <hr>
        </div>
        <div class="form-group">
          <h2>Where do you need this?</h2>
          <select class="form-control">
            <option>Bangkok</option>
          </select>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-default btn-service-detail-send">Send</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection

@section('script')

@endsection
