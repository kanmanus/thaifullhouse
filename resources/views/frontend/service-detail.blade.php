@extends('frontend.layout.main-layout')

@section('title', ' - Home')

@section('css')
  {!! Html::style('css/frontend/home.css') !!}
@endsection

@section('content')
<div class="container-fluid section-bg-service-detail">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="bg-service-caption">
          <h1>FIND AIRCONDITIONAL SERVICE</h1>
          <h3>REPAIR AND INSTALLATION CONTRACTORS</h3>
          <a href="" class="btn btn-default btn-request-quotes">
            Request Free Quotes
            <img src="images/icon/play-icon.png" alt="">
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container section-service-quality">
  <div class="row">
    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
      <h1>มั่นใจฝีมือ และความเป็นมืออาชีพ</h1>
      <h4>เราคัดสรรเฉพาะช่างฝีมือคุณภาพให้คุณ</h4>
      <div class="service-icon">
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
          <img src="images/icon/service-icon-1.png" alt="" class="img-responsive">
          <h4>Thai Full House Verified</h4>
          <p>ช่างทุกคนผ่านการอบรมปละได้รับใบรับรอง จาก Thai Full House</p>
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
          <img src="images/icon/service-icon-2.png" alt="" class="img-responsive">
          <h4>Safety Verified Thai Full House</h4>
          <p>มีข้อมูลบัตรประชาชนช่างทุกคน</p>
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
          <img src="images/icon/service-icon-3.png" alt="" class="img-responsive">
          <h4>Criminal Record Verified</h4>
          <p>ตรวจสอบประวัติอาชญากรรมจากสำนักงานตำรวจแห่งชาติแล้ว</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid section-people-saying">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h1>Hear What People Are Saying</h1>

        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <div class="outer-box-people">
                <div class="inner-box-people">
                  <img src="images/avatar.png" alt="">
                  <h3>makes the process of choosing between cleaners incredibly simple.</h3>
                  <h4>: Bryan</h4>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="outer-box-people">
                <div class="inner-box-people">
                  <img src="images/avatar.png" alt="">
                  <h3>makes the process of choosing between cleaners incredibly simple.</h3>
                  <h4>: Bryan</h4>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="outer-box-people">
                <div class="inner-box-people">
                  <img src="images/avatar.png" alt="">
                  <h3>makes the process of choosing between cleaners incredibly simple.</h3>
                  <h4>: Bryan</h4>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<div class="container section-matching-services">
  <div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
      <h1>Others Services</h1>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 matching-item">
      <a href="#">
        <img src="images/air-condition.png" alt="test" class="img-responsive matching-item-image">
        <div class="matching-icon-text">
          <img src="images/icon/เครื่องปรับอากาศ.png" alt="" class="img-responsive matching-icon">
          <h2 class="text-air">Air Conditioning</h2>
        </div>
      </a>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 matching-item">
      <a href="#">
        <img src="images/cleaning-image.png" alt="test" class="img-responsive matching-item-image">
        <div class="matching-icon-text">
          <img src="images/icon/cleaning.png" alt="" class="img-responsive matching-icon">
          <h2>Cleaning</h2>
        </div>
      </a>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 matching-item">
      <a href="#">
        <img src="images/electrical-image.png" alt="test" class="img-responsive matching-item-image">
        <div class="matching-icon-text">
          <img src="images/icon/electricity.png" alt="" class="img-responsive matching-icon">
          <h2>Electricity</h2>
        </div>
      </a>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 matching-item">
      <a href="#">
        <img src="images/plumber-image.png" alt="test" class="img-responsive matching-item-image">
        <div class="matching-icon-text">
          <img src="images/icon/plumber.png" alt="" class="img-responsive matching-icon">
          <h2>Plumber</h2>
        </div>
      </a>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 matching-item">
      <a href="#">
        <img src="images/moving-image.png" alt="test" class="img-responsive matching-item-image">
        <div class="matching-icon-text">
          <img src="images/icon/moving.png" alt="" class="img-responsive matching-icon">
          <h2>Moving</h2>
        </div>
      </a>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 matching-item">
      <a href="#">
        <img src="images/laundry-image.png" alt="test" class="img-responsive matching-item-image">
        <div class="matching-icon-text">
          <img src="images/icon/เครื่องซักผ้า.png" alt="" class="img-responsive matching-icon">
          <h2>Laundry</h2>
        </div>
      </a>
    </div>
  </div>
</div>
@endsection

@section('script')

@endsection
