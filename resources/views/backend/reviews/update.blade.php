<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')

@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Customer Name</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="customer_name_th" value="{{ $customer_name_th }}">
                            </div>
                            <div class="col-md-1">
                                ( TH )
                                </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Customer Name</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="customer_name_en" value="{{ $customer_name_en }}">
                            </div>
                            <div class="col-md-1">
                                ( EN )
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Title</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="customer_title_th" value="{{ $customer_title_th }}">
                            </div>
                            <div class="col-md-1">
                                ( TH )
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Title</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="customer_title_en" value="{{ $customer_title_en }}">
                            </div>
                            <div class="col-md-1">
                                ( EN )
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Description ( TH )</label>
                            <div class="col-md-9">
                                <textarea class="ckeditor form-control" name="description_th" rows="6" data-error-container="#editor2_error">{{ $description_th }}</textarea>
                                <div id="editor2_error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Description ( EN )</label>
                            <div class="col-md-9">
                                <textarea class="ckeditor form-control" name="description_en" rows="6" data-error-container="#editor2_error">{{ $description_en }}</textarea>
                                <div id="editor2_error">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
    {{ Html::script('assets/global/plugins/ckeditor/ckeditor.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/role/validation.js') }}
    <script>
        FormValidation.init();
    </script>
@endsection
