<?php
$table = $obj_model->table;
$primaryKey = $obj_model->primaryKey;
$fillable = $obj_model->fillable;

$a_param = Input::all();
$str_param = $obj_fn->parameter($a_param);

if(isset($data)) {
    foreach($fillable as $value){
        $$value = $data->$value;
    }
} else {
    foreach($fillable as $value){
        $$value = "";
    }
}
?>
@extends('backend.layout.main-layout')
@section('page-style')

@endsection
@section('more-style')
@endsection

@section('page-title')
    {{ $txt_manage.' '.$page_title }}
@endsection

@section('page-content')
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ url()->to($url_to) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="str_param" value="{{ $str_param }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Product Name</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="name_th" value="{{ $name_th }}">
                            </div>
                            <div class="col-md-4">
                                ( TH )
                                </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Product Name</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="name_en" value="{{ $name_en }}">
                            </div>
                            <div class="col-md-4">
                                ( EN )
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Type of Residence</label>
                            <div class="col-md-4">
                                <select class="form-control select2me" name="product_type_id">
                                    <option value="">Select...</option>
                                    @foreach( $product_type as $pro_type)
                                    <option value="{{$pro_type['product_type_id']}}">{{$pro_type->type}} = {{$pro_type['product_type_id']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Latitude</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="lat" value="{{ $lat }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Longitude</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="lng" value="{{ $lng }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Activity</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="activity" value="{{ $activity }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Price</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="price" value="{{ $price }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Per Period</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="per_period" value="{{ $per_period }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="description" value="{{ $description }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Bed</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="bed" value="{{ $bed }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Bath</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="bath" value="{{ $bath }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Level</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="level" value="{{ $level }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Area</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="area" value="{{ $area }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Area Unit</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="area_unit" value="{{ $area_unit }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="status" value="{{ $status }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                <button type="reset" class="btn default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    </div>
@endsection
@section('page-plugin')
    {{ Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}
    {{ Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}
@endsection
@section('more-script')
    {{ Html::script('js/backend/role/validation.js') }}
    <script>
        FormValidation.init();
    </script>
@endsection
